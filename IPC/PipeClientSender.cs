﻿using System.IO;
using System.IO.Pipes;

namespace IPC
{
    public class PipeClientSender
    {
        private readonly AnonymousPipeClientStream _clientStream;
        private readonly StreamWriter _writer;
        private bool _isClosed = false;

        /// <summary>
        /// 这个程序要放在ALC的运行目录, 程序启动时将Main函数(  public static void Main(string[] args) )的 args[0] 传入构造函数
        /// </summary>
        /// <param name="parentPipeHandle">
        ///  Parent 程序的Handle
        /// </param>
        public PipeClientSender(string parentPipeHandle)
        {
            _clientStream = new AnonymousPipeClientStream(PipeDirection.Out, parentPipeHandle);
            _writer = new StreamWriter(_clientStream)
            {
                AutoFlush = true
            };
        }

        /// <summary>
        /// 将ouput[0]传进来, 发送给ALC
        /// </summary>
        /// <param name="data"></param>
        public void SendData(double[] data)
        {
            var line = string.Join(",", data);
            _writer.WriteLine(line);
        }

        // 在程序退出是调用, 或者在获取数据最外的循环外try catch, 然后传入Exception.Message调用
        public void Close(string errorMessage = null)
        {
            if (_isClosed) return;
            try
            {
                _writer.WriteLine(errorMessage ?? "Exit");
                _writer.Close();
                _clientStream.Close();
            }
            finally
            {
                _isClosed = true;
            }
        }
    }
}