﻿using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Linq;

namespace IPC
{
    public class PipeServerReceiver
    {
        #region private fileds

        private readonly AnonymousPipeServerStream _serverStream;
        private readonly StreamReader _reader;
        private readonly Process _clientProcess;
        

        #endregion


        public string LastError { get; set; }
        

        public PipeServerReceiver(string chileProcessName)
        {
            _serverStream = new AnonymousPipeServerStream(PipeDirection.In, HandleInheritability.Inheritable);

            _clientProcess = new Process
            {
                StartInfo =
                {
                    FileName = chileProcessName,
                    Arguments = _serverStream.GetClientHandleAsString(),
                    UseShellExecute = false
                }
            };
            _clientProcess.Start();
            
            _serverStream.DisposeLocalCopyOfClientHandle();
            _reader = new StreamReader(_serverStream);
        }


        /// <summary>
        /// Return data of one trigger
        /// </summary>
        /// <returns>null on client exits or errors</returns>
        public double[] ReadData()
        {
            while (true)
            {
                var line = _reader.ReadLine();
                if (line == null) continue;
                
                if (line == "Exit" || line.StartsWith("Error"))
                {
                    LastError = line;
                    return null;
                }

                var data = line.Split(',').Select(double.Parse).ToArray();
                return data;
            }
        }
        
        public void Close()
        {
            _clientProcess.WaitForExit();
            _clientProcess.Close();
            
            _reader.Close();
            _serverStream.Close();
        }
        
    }
}