﻿using System;
using System.Linq;
using System.Threading;
using IPC;

namespace PipeClientSenderDemo
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            bool shouldError = true;

            var parentName = args[0];
            var clientPipe = new PipeClientSender(parentName);

            for (int i = 0; i < 10; i++)
            {
                var start = DateTime.Now.Millisecond;
                var data = Enumerable.Range(start, 10).Select(Convert.ToDouble).ToArray();

                clientPipe.SendData(data);
                Thread.Sleep(1500);
            }


            clientPipe.Close(shouldError ? "Error: {something bad happened}" : null);
        }
    }
}