﻿using System;
using System.Buffers;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Order;
using Microsoft.Diagnostics.Tracing.Parsers.AspNet;

namespace Benchmarking
{

    /// <summary>
    /// 对比各种裁剪图片方法的性能
    /// 1. 用Bitmap进行裁剪
    /// 2. 使用Unsafe进行遍历拷贝
    /// 3. 使用Buffer.BlockCopy
    /// 4. 使用Buffer.BlockCopy并且使用ArrayPool
    /// </summary>
    [MemoryDiagnoser]
    [Orderer(SummaryOrderPolicy.FastestToSlowest)]
    [RankColumn]
    public class ImageCropBenchmarking
    {
        private Bitmap _inputBitmap;
        private byte[] _inputBytes;
        private const int ImageWidth = 4000;
        private const int ImageHeight = 3000;
        private const int X = 100;
        private const int Y = 100;
        private const int SubWidth = 500;
        private const int SubHeight = 500;
        private const int RoiSize = SubHeight * SubWidth;
        private readonly ArrayPool<byte> _arrayPool = ArrayPool<byte>.Create(RoiSize+10, 10);

        [GlobalSetup]
        public void Setup()
        {

            var imageSize = ImageWidth * ImageHeight;
            _inputBytes = new byte[imageSize];
            _inputBitmap = new Bitmap(ImageWidth, ImageHeight, PixelFormat.Format8bppIndexed);
            var mem = _arrayPool.Rent(SubWidth * SubHeight);
            _arrayPool.Return(mem);
        }

        [Benchmark]
        public void CropWithBitmap()
        {
            var output = ImageCutCore(_inputBitmap, X, Y, SubWidth, SubHeight);
        }

        [Benchmark]
        public void CropWithUnsafeOp_WithoutArrayPool()
        {
            byte[] outputBytes = null;
            CropUnsafe(_inputBytes, ImageWidth, X, Y, SubWidth, SubHeight, ref outputBytes);
        }

        [Benchmark]
        public void CropWithBlockCopy_WithoutArrayPool()
        {
            byte[] outputBytes = null;
            CropBlockCopy(_inputBytes, ImageWidth, X, Y, SubWidth, SubHeight, ref outputBytes);
        }

        [Benchmark]
        public void CropWithBlockCopy_WithArrayPool()
        {
            byte[] outputBytes = _arrayPool.Rent(SubWidth * SubHeight);
            CropBlockCopy(_inputBytes, ImageWidth, X, Y, SubWidth, SubHeight, ref outputBytes);
            _arrayPool.Return(outputBytes);
        }

        public static unsafe void CropUnsafe(byte[] input, int imageWidth, int x, int y, int subWidth, int subHeight, ref byte[] output)
        {
            var shouldNewArray = (output?.Length >= subWidth * subHeight) != true;
            if (shouldNewArray) output = new byte[subWidth * subHeight];
            fixed (byte* array = input)
            {
                fixed (byte* outputArray = output)
                {
                    for (int row = y; row < subHeight + y; row++)
                    {
                        var srcStart = row * imageWidth + x;
                        var dstStart = (row - y) * subWidth;
                        for (int col = 0; col < subWidth; col++)
                        {
                            var srcCurrent = srcStart + col;
                            var dstCurrent = dstStart + col;
                            var srcValue = array[srcCurrent];
                            outputArray[dstCurrent] = srcValue;
                        }
                    }
                }
            }

        }


        public static void CropBlockCopy(byte[] input, int imageWidth, int x, int y, int subWidth, int subHeight,
            ref byte[] output)
        {
            var shouldNewArray = (output?.Length >= subWidth * subHeight) != true;
            if (shouldNewArray) output = new byte[subWidth * subHeight];

                    for (int row = y; row < subHeight + y; row++)
                    {
                        var srcStart = row * imageWidth + x;
                        var dstStart = (row - y) * subWidth;
                        Buffer.BlockCopy(input, srcStart, output, dstStart, subWidth);
                    }
               
        }

        public static Bitmap ImageCutCore(Bitmap bitmap, int x, int y, int width, int height)
        {
            var cropRect = new Rectangle(x, y, width, height);
            var target = new Bitmap(cropRect.Width, cropRect.Height);
        
            using (Graphics g = Graphics.FromImage(target))
            {
                g.DrawImage(bitmap, new Rectangle(0, 0, target.Width, target.Height),
                    cropRect,
                    GraphicsUnit.Pixel);
            }
            return target;
        }
    }
}