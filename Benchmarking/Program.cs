﻿using System;
using BenchmarkDotNet.Running;

namespace Benchmarking
{
    internal class Program
    {
        static void Main(string[] args)
        {
            BenchmarkRunner.Run<ElementAccess>();
            //var obj = new ImageCropBenchmarking();
            //obj.Setup();
            //obj.CropWithUnsafeOp_WithMemoryPool();
        }
    }
}
