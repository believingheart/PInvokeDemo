﻿using System.Collections.Generic;
using System.Linq;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Order;

namespace Benchmarking
{
    [MemoryDiagnoser]
    [Orderer(SummaryOrderPolicy.FastestToSlowest)]
    [RankColumn]
    public class ElementAccess
    {
        private IEnumerable<string> _trueEnumerable;
        private IEnumerable<string> _fakeEnumerable;
        private IReadOnlyList<string> _list;
        const int Size = 100000;
        private const int End = Size - 1;

        [GlobalSetup]
        public void Setup()
        {
            _trueEnumerable = Enumerable.Range(0, Size).Select(n=>n.ToString());
            _list = _trueEnumerable.ToArray();
            _fakeEnumerable = _list;
        }


        [Benchmark]
        public void IndexBasedAccess()
        {
            var ele = _list[End];
        }


        [Benchmark]
        public void TrueEnumerableBasedAccess()
        {
            var ele = _trueEnumerable.ElementAt(End);
        }

        [Benchmark]
        public void FakeEnumerableBasedAccess()
        {
            var ele = _fakeEnumerable.ElementAt(End);
        }
    }
}