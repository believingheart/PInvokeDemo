﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfDrawPieDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static readonly DependencyProperty PieChartDataProperty = DependencyProperty.Register("PieChartData", typeof(Dictionary<string, double>), typeof(MainWindow), new PropertyMetadata(default(Dictionary<string, double>)));

        public MainWindow()
        {
            InitializeComponent();
            Loaded += (sender, args) =>
            {
                PieChartData = new Dictionary<string, double>()
                {
                    ["Milk"] = 5,
                    ["Egg"] = 3
                };
            };
        }

        public Dictionary<string, double> PieChartData
        {
            get { return (Dictionary<string, double>) GetValue(PieChartDataProperty); }
            set { SetValue(PieChartDataProperty, value); }
        }

    }
}