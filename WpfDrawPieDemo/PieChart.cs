﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfDrawPieDemo
{
    /// <summary>
    /// Follow steps 1a or 1b and then 2 to use this custom control in a XAML file.
    ///
    /// Step 1a) Using this custom control in a XAML file that exists in the current project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:WpfDrawPieDemo"
    ///
    ///
    /// Step 1b) Using this custom control in a XAML file that exists in a different project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:WpfDrawPieDemo;assembly=WpfDrawPieDemo"
    ///
    /// You will also need to add a project reference from the project where the XAML file lives
    /// to this project and Rebuild to avoid compilation errors:
    ///
    ///     Right click on the target project in the Solution Explorer and
    ///     "Add Reference"->"Projects"->[Browse to and select this project]
    ///
    ///
    /// Step 2)
    /// Go ahead and use your control in the XAML file.
    ///
    ///     <MyNamespace:CustomControl1/>
    ///
    /// </summary>
    [TemplatePart(Name = "PART_Canvas")]
    public class PieChart : Control
    {
        static PieChart()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PieChart), new FrameworkPropertyMetadata(typeof(PieChart)));
        }

        public static readonly DependencyProperty DataProperty = DependencyProperty.Register(
            "Data", typeof(Dictionary<string, double>), typeof(PieChart),
            new PropertyMetadata(default(Dictionary<string, double>), OnPieChartDataPropertyChanged));

        private static void OnPieChartDataPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var view = (PieChart) d;
            var newVal = (Dictionary<string, double>) e.NewValue;
            if (newVal == null) return;
            view.DrawPieChart(newVal);
        }

        private void DrawPieChart(Dictionary<string, double> data)
        {
            // Calc portions
            var sum = data.Values.Sum();
            var portions = data.Values.Select(v => v / sum).ToArray();
            var cumulativePortions = CalcCumulativePortions(portions);

            // Gen angle sections
            var totalAngle = 2 * Math.PI;
            var angleSectionsStart = cumulativePortions.Select(p => p * totalAngle).Take(data.Count).ToArray();
            var angleSectionsEnd = cumulativePortions.Select(p => p * totalAngle).Skip(1).ToArray();

            // Gen colors
            var colorWheel = new ColorWheel();
            var colorBrushes = data.Select(d => colorWheel.NextColorBrush()).ToArray();

            DrawPieChart(data, angleSectionsStart, angleSectionsEnd, colorBrushes);
        }

        private void DrawPieChart(Dictionary<string, double> data, double[] angleSectionsStart,
            double[] angleSectionsEnd, SolidColorBrush[] colorBrushes)
        {
            var canvas = (Canvas) Template.FindName("PART_Canvas", this);

            Point pt1;
            Point pt2;
            for (int i = 0; i < data.Count; i++)
            {
                var slice = canvas.DrawPieSlice(colorBrushes[i], new SolidColorBrush(Colors.Black), 1,
                    new Rect(100, 100, 200, 200), angleSectionsStart[i], angleSectionsEnd[i], false,
                    SweepDirection.Clockwise, out pt1, out pt2);
                slice.StrokeLineJoin = PenLineJoin.Round;
            }
        }

        public static Rect BoundsRelativeTo(FrameworkElement element,
            Visual relativeTo)
        {
            return
                element.TransformToVisual(relativeTo)
                    .TransformBounds(LayoutInformation.GetLayoutSlot(element));
        }

        public static T FindParent<T>(FrameworkElement inputElement, int maxLevels = 100) where T : DependencyObject
        {
            var level = 0;
            while (level < maxLevels)
            {
                level++;
                var parent = inputElement.Parent;
                if (parent is T dependencyObject) return dependencyObject;
            }

            return null;
        }

        private List<double> CalcCumulativePortions(double[] portions)
        {
            var output = new List<double>() {0};
            for (int i = 0; i < portions.Length; i++)
            {
                var partialSum = portions.Take(i + 1).Sum();
                output.Add(partialSum);
            }

            return output;
        }

        public Dictionary<string, double> Data
        {
            get { return (Dictionary<string, double>) GetValue(DataProperty); }
            set { SetValue(DataProperty, value); }
        }
    }
}