﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using BitMiracle.LibTiff.Classic;

namespace ImageMiscDemo
{
    unsafe class UnsafeArrayValidator
    {
        private readonly float* _inputPtr;
        private readonly float* _outputPtr;
        private readonly int _assignCount;
        private readonly float _nanResult;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputPtr"></param>
        /// <param name="outputPtr"></param>
        /// <param name="assignCount">How many data to assign in a single call of <see cref="ValidatePart"/></param>
        /// <param name="nanResult"></param>
        public UnsafeArrayValidator(float* inputPtr, float* outputPtr, int assignCount, float nanResult)
        {
            _inputPtr = inputPtr;
            _outputPtr = outputPtr;
            _assignCount = assignCount;
            _nanResult = nanResult;
        }

        /// <summary>
        /// Convert nan values to <see cref="_nanResult"/> in part of array
        /// </summary>
        /// <param name="parallelIndex"></param>
        public void ValidatePart(int parallelIndex)
        {
            var offset = _assignCount * parallelIndex;
            ValidateNanValuesSingleThread(_inputPtr, _outputPtr, _nanResult, offset, _assignCount);
        }

        /// <summary>
        /// Copy values from <see cref="inputArray"/> to <see cref="outputArray"/>,
        /// nan values will be convert to <see cref="nanReuslt"/>
        /// </summary>
        /// <param name="inputArray"></param>
        /// <param name="outputArray"></param>
        /// <param name="nanReuslt"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        public static void ValidateNanValuesSingleThread(float* inputArray, float* outputArray,
            float nanReuslt,
            int offset,
            int count)
        {
            for (int index = 0; index < count; index++)
            {
                var valueValidated = float.IsNaN(inputArray[offset + index])
                    ? nanReuslt
                    : inputArray[offset + index];
                outputArray[index] = valueValidated;
            }
        }
    }
    internal class Program
    {
        private static void Main()
        {
            var stopWatch = Stopwatch.StartNew();
            var data = TiffFileToFloatArray(@"C:\Users\PanXiaojin\Downloads\2020-04-04_14-21-51-203.tif", false);
            Console.WriteLine($"Read cost: {stopWatch.ElapsedMilliseconds} ms");
            var index = 2646 * 2085 + 1868;
            var value = data[index];

            stopWatch.Restart();
            float[] validatedArray = new float[data.Length];

            // Unmanaged
            ValidateNanValues(data, validatedArray, 1000, 1);
            var msValidate = stopWatch.ElapsedMilliseconds;
            Console.WriteLine($"Validation cost: {msValidate} ms");
            
            // Managed
            stopWatch.Restart();
            validatedArray = data.Select(d => float.IsNaN(d) ? 1000 : d).ToArray();
             msValidate = stopWatch.ElapsedMilliseconds;
             Console.WriteLine($"Managed validation cost: {msValidate} ms");

        }

        /// <summary>
        /// Copy values from <see cref="input"/> to <see cref="output"/>,
        /// nan values will be convert to <see cref="nanResult"/>
        /// /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        /// <param name="nanResult"></param>
        /// <param name="numThreads"></param>
        /// <exception cref="InvalidOperationException"></exception>
        private static unsafe void ValidateNanValues(float[] input, float[] output,
            float nanResult,
            int numThreads = 1)
        {
            var arrayLength = input.Length;
            if (arrayLength % numThreads != 0)
                throw new InvalidOperationException(
                    $"Can not divide array(size:{arrayLength} by numThreads(size:{numThreads})");
    

            fixed (float* ptr = &input[0])
            {
                fixed (float* outPtr = &output[0])
                {
                    if (numThreads <= 1) UnsafeArrayValidator.ValidateNanValuesSingleThread(ptr, outPtr, nanResult, 0, arrayLength);
                    else
                    {
                        var batchSize = arrayLength / numThreads;
                        var validator = new UnsafeArrayValidator(ptr, outPtr, (int) batchSize, nanResult);
                        Parallel.For(0, numThreads, validator.ValidatePart);
                    }
                }
            }

        }

        /// <summary>
        /// Read tiff image with pixel type(float)
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="convertNanValues">whether nan values should be converted</param>
        /// <param name="nanValueResult"></param>
        /// <returns>image data</returns>
        private static unsafe float[] TiffFileToFloatArray(string filePath, bool convertNanValues,
            float nanValueResult = -100)
        {
            using (Tiff image = Tiff.Open(filePath, "r"))
            {
                if (image == null)
                {
                    return null;
                }

                // Find the width and height of the image
                FieldValue[] value = image.GetField(TiffTag.IMAGEWIDTH);
                int width = value[0].ToInt();

                value = image.GetField(TiffTag.IMAGELENGTH);
                int height = value[0].ToInt();

                int imageSize = height * width;
                var pixelSize = sizeof(float);
                var imageData = new float[imageSize];
                var imageDataRaw = new byte[imageSize * pixelSize];

                for (int row = 0; row < height; row++)
                {
                    image.ReadRawStrip(row, imageDataRaw, row * width * 4, width * 4);
                }

                var convertMethod = convertNanValues
                    ? (Func<byte[], int, float>) ((arr, startIndex) =>
                    {
                        var valueUnchanged = BitConverter.ToSingle(arr, startIndex);
                        return float.IsNaN(valueUnchanged) ? nanValueResult : valueUnchanged;
                    })
                    : BitConverter.ToSingle;

                fixed (float* ptr = &imageData[0])
                {
                    for (int i = 0; i < imageData.Length; i++)
                    {
                        var start = i * 4;
                        ptr[i] = convertMethod(imageDataRaw, start);
                    }
                }
                
                return imageData;
            }
        }



    }

}