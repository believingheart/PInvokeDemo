﻿using System.Windows;
using System.Windows.Controls;

namespace WpfBehaviorDemo
{
    public class AttachedProperties
    {
        public static readonly DependencyProperty SelectAllOnFocusProperty = DependencyProperty.RegisterAttached(
            "SelectAllOnFocus", typeof(bool), typeof(AttachedProperties), new UIPropertyMetadata(default(bool), OnSelectAllOnFocusPropertyChanged));

        private static void OnSelectAllOnFocusPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var textBox = (TextBox) d;
            textBox.GotFocus += (sender, args) =>
            {
                Application.Current.Dispatcher?.InvokeAsync(() =>
                {
                    textBox.SelectAll();
                });
            };
        }

        public static void SetSelectAllOnFocus(DependencyObject element, bool value)
        {
            element.SetValue(SelectAllOnFocusProperty, value);
            var textBox = (TextBox) element;
            textBox.GotFocus += (sender, args) =>
            {
                Application.Current.Dispatcher?.InvokeAsync(() =>
                {
                    textBox.SelectAll();
                });
            };
        }

        public static bool GetSelectAllOnFocus(DependencyObject element)
        {
            return (bool) element.GetValue(SelectAllOnFocusProperty);
        }
        
        
    }
}