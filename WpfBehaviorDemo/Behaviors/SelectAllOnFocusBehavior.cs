﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace WpfBehaviorDemo.Behaviors
{
    public class SelectAllOnFocusBehavior : Behavior<TextBox>
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.GotFocus += (sender, args) =>
            {
                Application.Current.Dispatcher?.InvokeAsync(() =>
                {
                    AssociatedObject.SelectAll();
                });
            };
        }
        
    }
}