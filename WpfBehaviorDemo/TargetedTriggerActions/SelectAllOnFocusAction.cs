﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace WpfBehaviorDemo.TargetedTriggerActions
{
    public class SelectAllOnFocusAction : TargetedTriggerAction<TextBox>
    {
        protected override void Invoke(object parameter)
        {
            var textBox = Target;
            Application.Current.Dispatcher?.InvokeAsync(() =>
            {
                textBox.SelectAll();

            });
        }
    }
}