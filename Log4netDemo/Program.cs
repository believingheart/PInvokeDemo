﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Log4netDemo
{
    internal class Program
    {
        public static void Main(string[] args)
        {

            var loggerNames = new string[] { "logger1", "logger2"};
            var appenderParams = loggerNames.Select(n => new AppenderParam(n)).ToArray();
            var loggers = RollingFileAppenderManager.Config(appenderParams, Directory.GetCurrentDirectory());

            var logger1 = loggers[loggerNames[0]];
            var logger2 = loggers[loggerNames[1]];

            var stopWatch = Stopwatch.StartNew();
            for (int i = 0; i < 1; i++)
            {
                var index = i;
                var message1 = $"Debug log1 {index}";
                logger1.Log(message1);
                
                var message2 = $"Debug log2 {index}";
                logger2.Log(message2);
            }

            Console.WriteLine($"{stopWatch.ElapsedMilliseconds}");
            
            // stopWatch.Restart();
            // for (int i = 0; i < 100000; i++)
            // {
            //     var index = i;
            //     var message = $"Debug log {index}";
            //     DebugLog(message);
            // }
            // Console.WriteLine($"{stopWatch.ElapsedMilliseconds}");


        }

        private static void DebugLog(string message)
        {
            File.AppendAllText("Log.txt", message);
        }
    }
}