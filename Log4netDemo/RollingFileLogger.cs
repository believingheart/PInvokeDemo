﻿using log4net;

namespace Log4netDemo
{
    public class RollingFileLogger
    {
        #region private fields

        private readonly ILog _log;

        #endregion

        #region ctor

        internal RollingFileLogger(ILog log)
        {
            _log = log;
        }

        #endregion

        #region api

        public void Log(string message)
        {
            _log.Debug(message);
        }

        #endregion
        
        
    }
}