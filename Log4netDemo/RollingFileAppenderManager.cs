﻿using System;
using System.Collections.Generic;
using System.IO;
using log4net;

namespace Log4netDemo
{
    public static class RollingFileAppenderManager
    {
        private static Dictionary<string, RollingFileLogger> _loggers;

        public static Dictionary<string, RollingFileLogger> Config(AppenderParam[] appenderParams, string directoryName)
        {
            if (_loggers != null)
                throw new InvalidOperationException("Config can only be called once during the whole application");

            _loggers = new Dictionary<string, RollingFileLogger>();

            //If the directory doesn't exist then create it
            Directory.CreateDirectory(directoryName);
            foreach (var appenderParam in appenderParams)
            {
                var loggerName = $"{appenderParam.AppenderName}Logger";

                var repoName = $"{appenderParam.AppenderName}Repository";
                var repository = LogManager.CreateRepository(repoName);


                //Add the default log appender if none exist

                var fileName = Path.Combine(directoryName, $"{appenderParam.AppenderName}.log");

                //Create the rolling file appender
                var appender = new log4net.Appender.RollingFileAppender
                {
                    Name = appenderParam.AppenderName,
                    File = fileName,
                    StaticLogFileName = true,
                    AppendToFile = false,
                    RollingStyle = log4net.Appender.RollingFileAppender.RollingMode.Size,
                    MaxSizeRollBackups = appenderParam.MaxSizeRollBackups,
                    MaximumFileSize = appenderParam.MaximumFileSize,
                    PreserveLogFileNameExtension = true
                };

                //Configure the layout of the trace message write
                var layout = new log4net.Layout.PatternLayout()
                {
                    ConversionPattern = "%message"
                };
                appender.Layout = layout;
                layout.ActivateOptions();

                //Let log4net configure itself based on the values provided
                appender.ActivateOptions();
                
                log4net.Config.BasicConfigurator.Configure(repository, appender);


                var iLog =LogManager.GetLogger(repoName, loggerName);
                _loggers[appenderParam.AppenderName] = new RollingFileLogger(iLog);
            }

            return _loggers;
        }
    }
}