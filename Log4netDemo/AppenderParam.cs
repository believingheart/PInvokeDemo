﻿namespace Log4netDemo
{
    public class AppenderParam
    {
        #region ctor

        public AppenderParam(string appenderName)
        {
            AppenderName = appenderName;
        }

        #endregion

        #region props

        public string AppenderName { get; }
        public string MaximumFileSize { get; set; } = "1MB";
        public int MaxSizeRollBackups { get; set; } = 10;

        #endregion
    }
}