﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Media.Imaging;

namespace IntptrBmp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            
    
            
            Loaded += OnLoaded;
        }

        public static void Test()
        {
            var 
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {

            var sourceBmp = new Bitmap("Image20-06-16_21-11-44_698.bmp");
            var stopWatch = Stopwatch.StartNew();
            var ptr = GrayScaleBitmapToPtr(sourceBmp);

            var bmpRecovered = PtrToBmp(ptr, 5120, 5120);
            Debug.WriteLine($"Recover time: {stopWatch.ElapsedMilliseconds}ms");

            var imageSource = BitmapToImageSource(bmpRecovered);
            
            bmpRecovered.Save("bmpRecovered.bmp");
            ImageControl.Source = imageSource;
       
            

        }

        private unsafe Bitmap PtrToBmp(IntPtr sourcePtr, int width, int height)
        {
            var size = width * height;
            byte[] srcArray = new byte[size];
            Marshal.Copy(sourcePtr, srcArray, 0, size);     
            Bitmap bmp = new Bitmap(width, height, PixelFormat.Format8bppIndexed);

            //map pixels to colors...
            BitmapData bitmapData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height),
                ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);

            IntPtr dstPtr = bitmapData.Scan0;

            //if you dont want to use pointers
            for (int y = 0; y < bmp.Height; y++)
            {
                for (int x = 0; x < bmp.Width; x++)
                {
                    //initialize a low contrast image
                    System.Runtime.InteropServices.Marshal.WriteByte(dstPtr, y * bitmapData.Stride + x, srcBmpData[y*width+x]);
                }
            }

            bmp.UnlockBits(bitmapData);

            //get and fillup a grayscale-palette
            ColorPalette pal = bmp.Palette;

            for (int i = 0; i < 256; i++)
            {
                pal.Entries[i] = Color.FromArgb(255, i, i, i);
            }

            bmp.Palette = pal;
            


            return bmp;
        }
        
        private BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }
        
        public static unsafe IntPtr GrayScaleBitmapToPtr(Bitmap bmp)
        {
            // var size = bmp.Height * bmp.Width;
            // var unmanagedPointer = Marshal.AllocHGlobal(size);
            // var dstArray = (byte*)unmanagedPointer.ToPointer();

            //map pixels to colors...
            var bitmapData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

            var p = bitmapData.Scan0;
            // var sourceArray = (byte*)p.ToPointer();
            //
            // var managedArray = new byte[size];
            // Marshal.Copy(p, managedArray, 0, size);
            //
            // //if you dont want to use pointers
            // for (var y = 0; y < bmp.Height; y++)
            // {
            //     for (var x = 0; x < bmp.Width; x++)
            //     {
            //         var index = y * bmp.Width + x;
            //         dstArray[index] = sourceArray[index];
            //         //initialize a low contrast image
            //         // System.Runtime.InteropServices.Marshal.WriteByte(unmanagedPointer, y*bmp.Width+x, managedArray[y * bitmapData.Stride + x]);
            //     }
            // }
            
            

            // bmp.UnlockBits(bitmapData);

            return p;
        }
    }
}