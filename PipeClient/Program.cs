﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Threading;

namespace PipeClient
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                using (PipeStream pipeClient =
                    new AnonymousPipeClientStream(PipeDirection.Out, args[0]))
                {
                    Console.WriteLine("[CLIENT] Current TransmissionMode: {0}.",
                       pipeClient.TransmissionMode);

                    using (var sw = new StreamWriter(pipeClient))
                    {
                        sw.AutoFlush = true;
                        // Display the read text to the console
                        string temp;

                        // Wait for 'sync message' from the server.
                        var data = Enumerable.Range(1, 200).Select(ele => (double) ele);
                        

                        for (int i = 0; i < 10; i++)
                        {
                            var clock = Stopwatch.StartNew();
                            var enumerable = data as double[] ?? data.ToArray();
                            var line = string.Join(",", enumerable);
                            Console.WriteLine($"Pack time: {clock.ElapsedMilliseconds} ms");
                            sw.WriteLine(line);
                            pipeClient.WaitForPipeDrain();
                            Thread.Sleep(1000);
                        }
                        sw.WriteLine("Exit");
                    }
                }
            }
            Console.Write("[CLIENT] Press Enter to continue...");
            Console.ReadLine();
        }
    }
}