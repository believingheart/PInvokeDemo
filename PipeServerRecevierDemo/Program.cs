﻿using System;
using System.Linq;
using IPC;

namespace PipeServerRecevierDemo
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var pipeServer = new PipeServerReceiver("PipeClientSenderDemo.exe");

            while (true)
            {
                var data = pipeServer.ReadData();
                
                if(data == null)
                {
                    var error = pipeServer.LastError;
                    Console.WriteLine($"Client errored: {error}");
                    break;
                }
                
                Console.WriteLine($"Received average: {data.Average()}");
            }
            
            pipeServer.Close();

            Console.ReadKey();

        }
    }
}