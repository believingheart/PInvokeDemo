﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Linq;

namespace PipeServer
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Process pipeClient = new Process();

            pipeClient.StartInfo.FileName = "PipeClient.exe";

            using (AnonymousPipeServerStream pipeServer =
                new AnonymousPipeServerStream(PipeDirection.In,
                    HandleInheritability.Inheritable))
            {
                Console.WriteLine("[SERVER] Current TransmissionMode: {0}.",
                    pipeServer.TransmissionMode);

                // Pass the client process a handle to the server.
                pipeClient.StartInfo.Arguments =
                    pipeServer.GetClientHandleAsString();
                pipeClient.StartInfo.UseShellExecute = false;
                pipeClient.Start();

                pipeServer.DisposeLocalCopyOfClientHandle();

                try
                {
                    // Read user input and send that to the client process.
                    using (var sr = new StreamReader(pipeServer))
                    {
                        while (true)
                        {
                            var messageFromClient = sr.ReadLine() ?? "null";
                            if (messageFromClient == "null")
                            {
                                Debugger.Break();
                                break;
                            }
                            if (messageFromClient == "Exit") break;
                            var clock = Stopwatch.StartNew();
                            var data = messageFromClient.Split(',').Select(ele => double.Parse(ele));
                            var ms = clock.ElapsedMilliseconds;
                            var average = data.Average();
                            Console.WriteLine($"Received from client:{average}, unpack time:{ms} ms");
                      
                            // Send the console input to the client process.
                        }
                        
                    }
                }
                // Catch the IOException that is raised if the pipe is broken
                // or disconnected.
                catch (IOException e)
                {
                    Console.WriteLine("[SERVER] Error: {0}", e.Message);
                }
            }

            pipeClient.WaitForExit();
            pipeClient.Close();
            Console.WriteLine("[SERVER] Client quit. Server terminating.");
        }
    }
}