﻿using System.Windows;
using System.Windows.Controls;

namespace CustomControlDemo.ClearableTextBox
{
    /// <summary>
    /// Follow steps 1a or 1b and then 2 to use this custom control in a XAML file.
    ///
    /// Step 1a) Using this custom control in a XAML file that exists in the current project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:CustomControlDemo"
    ///
    ///
    /// Step 1b) Using this custom control in a XAML file that exists in a different project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:CustomControlDemo;assembly=CustomControlDemo"
    ///
    /// You will also need to add a project reference from the project where the XAML file lives
    /// to this project and Rebuild to avoid compilation errors:
    ///
    ///     Right click on the target project in the Solution Explorer and
    ///     "Add Reference"->"Projects"->[Browse to and select this project]
    ///
    ///
    /// Step 2)
    /// Go ahead and use your control in the XAML file.
    ///
    ///     <MyNamespace:CustomControl1/>
    ///
    /// </summary>
    /// 
    [TemplatePart(Name = "Part_ClearButton", Type = typeof(Button))]
    public class ClearableTextBox : TextBox
    {
        static ClearableTextBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ClearableTextBox), new FrameworkPropertyMetadata(typeof(ClearableTextBox)));
        }



        public object ClearButtonContent
        {
            get { return (object)GetValue(ClearButtonContentProperty); }
            set { SetValue(ClearButtonContentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ClearButtonContent.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ClearButtonContentProperty =
            DependencyProperty.Register("ClearButtonContent", typeof(object), typeof(ClearableTextBox), new PropertyMetadata(null));

        public override void OnApplyTemplate()
        {
  
            base.OnApplyTemplate();
            var clearButton = (Button)Template.FindName("Part_ClearButton", this);
            clearButton.Click += ClearButton_Click;
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }
    }
}
