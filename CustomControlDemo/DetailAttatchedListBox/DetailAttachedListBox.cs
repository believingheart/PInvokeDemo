﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CustomControlDemo.DetailAttatchedListBox
{
    /// <summary>
    /// Follow steps 1a or 1b and then 2 to use this custom control in a XAML file.
    ///
    /// Step 1a) Using this custom control in a XAML file that exists in the current project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:CustomControlDemo.DetailAttatchedListBox"
    ///
    ///
    /// Step 1b) Using this custom control in a XAML file that exists in a different project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:CustomControlDemo.DetailAttatchedListBox;assembly=CustomControlDemo.DetailAttatchedListBox"
    ///
    /// You will also need to add a project reference from the project where the XAML file lives
    /// to this project and Rebuild to avoid compilation errors:
    ///
    ///     Right click on the target project in the Solution Explorer and
    ///     "Add Reference"->"Projects"->[Browse to and select this project]
    ///
    ///
    /// Step 2)
    /// Go ahead and use your control in the XAML file.
    ///
    ///     <MyNamespace:CustomControl1/>
    ///
    /// </summary>
    public class DetailAttachedListBox : ListBox
    {
        static DetailAttachedListBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(DetailAttachedListBox), new FrameworkPropertyMetadata(typeof(DetailAttachedListBox)));
        }



        public DataTemplate DetailTemplate
        {
            get { return (DataTemplate)GetValue(DetailTemplateProperty); }
            set { SetValue(DetailTemplateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DetailTemplate.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DetailTemplateProperty =
            DependencyProperty.Register("DetailTemplate", typeof(DataTemplate), typeof(DetailAttachedListBox), new PropertyMetadata(null));




        public GridLength ListBoxColumnWidth
        {
            get { return (GridLength)GetValue(ListBoxColumnWidthProperty); }
            set { SetValue(ListBoxColumnWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ListBoxColumnWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ListBoxColumnWidthProperty =
            DependencyProperty.Register("ListBoxColumnWidth", typeof(GridLength), typeof(DetailAttachedListBox), new UIPropertyMetadata(GridLength.Auto));



    }
}
