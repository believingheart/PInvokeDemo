﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Demo
{
    // Tells compiler that the order of fields in c# struct matches that 
    // of the cpp struct
    [StructLayout(LayoutKind.Sequential)]
    public struct Shoe
    {
       public int Id;
        [MarshalAs(UnmanagedType.BStr)]
       public string Color;
      public  double Size;
        [MarshalAs(UnmanagedType.BStr)]
        public string Brand;
    }
}
