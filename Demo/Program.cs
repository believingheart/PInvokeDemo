﻿using System;
using System.Runtime.InteropServices;

namespace Demo
{
    internal class Program
    {
        // Passing in and out primitive types
        [DllImport("NativeLibrary", EntryPoint = "Add")]
        public static extern int AddNumber(int a, int b);

        // Passing in string
        [DllImport("NativeLibrary")]
        public static extern int GetStringLength(string text);


        // Passing in string
        [DllImport("NativeLibrary")]
        public static extern string CreateStringFromStack();


        // Passing out string
        [DllImport("NativeLibrary")]
        [return: MarshalAs(UnmanagedType.BStr)]
        public static extern string ProperlyPassoutString();

        // Passing out unmanaged array
        [DllImport("NativeLibrary")]
        public static extern IntPtr Return2Double();

        // Release unmanaged array
        [DllImport("NativeLibrary")]
        public static extern void ReleaseDoubleArray(IntPtr ptr);

        // Passing struct to c++
        [DllImport("NativeLibrary")]
        public static extern void PrintShoe(Shoe shoe);

        // Returning struct from c++
        [DllImport("NativeLibrary")]
        public static extern Shoe CreateShoe(double param);     
        
        // Returning struct with pointer inside from c++
        [DllImport("NativeLibrary")]
        public static extern Polygon CreateRegion();     
        
        // Passing array of structs to c++
        [DllImport("NativeLibrary")]
        public static extern void ConsumePoints(IntPtr ptr);


        public static void Main(string[] args)
        
        {
            var sum = AddNumber(10, 20);
            Console.WriteLine($"Result from AddNumber = {sum}");

            //******************************************************************
            var numChar = GetStringLength("Hello");
            Console.WriteLine($"Result from GetStringLength = {numChar}");

            //********************************************************************
            // This will cause application to shut down
            //var stringFromUnmanagedStack = CreateStringFromStack();
            //Console.WriteLine($"Result from CreateStringFromStack = {stringFromUnmanagedStack}");

            // *****************************************************************
            var bString = ProperlyPassoutString();
            Console.WriteLine($"Result from ProperlyPassoutString = {bString}");

            // *****************************************************************
            var shoe = new Shoe { Brand = "Nike", Color = "Blue", Id = 114514, Size = 1919 };
            Console.WriteLine($"Result from PrintShoe = ");
            PrintShoe(shoe);

            // *****************************************************************
            // TODO: return a struct from cpp



            // *****************************************************************
            // Returning struct with pointer inside from c++
            var polygon = CreateRegion();
            MarshalUnmanagedArray2Struct<Point>(polygon.Points, polygon.PointCount, out var managedArray);
            for (var index = 0; index < managedArray.Length; index++)
            {
                var point = managedArray[index];
                Console.WriteLine($"The {index} point.X = {point.X}");
            }
            Marshal.DestroyStructure<Point>(polygon.Points);

            // *****************************************************************
            // Passing array of structs to c++
            var ptSize = Marshal.SizeOf<Point>();
            var ptr = Marshal.AllocHGlobal(ptSize * 2);
            var start = ptr.ToInt64();
            var pts = new Point[] {new Point() {X = 1}, new Point() {X = 2}};
            for (int ptIndex = 0; ptIndex < pts.Length; ptIndex++)
            {
                var current = new IntPtr(start);
                Marshal.StructureToPtr(pts[ptIndex], current, false);
                start += ptSize;
            }
            ConsumePoints(ptr);
            Marshal.FreeHGlobal(ptr);


            Console.WriteLine("Program ends successfully");
            Console.ReadKey();
        }


        public static void MarshalUnmanagedArray2Struct<T>(IntPtr unmanagedArray, int length, out T[] managedArray)
        {
            var size = Marshal.SizeOf(typeof(T));
            managedArray = new T[length];

            for (int i = 0; i < length; i++)
            {
                IntPtr ins = new IntPtr(unmanagedArray.ToInt64() + i * size);
                managedArray[i] = Marshal.PtrToStructure<T>(ins);
            }
        }
    }
}