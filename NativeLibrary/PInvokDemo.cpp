#include "pch.h"
#include <comdef.h>
#include <iostream>
#include <string>


struct Shoe {
	int Id;
	BSTR Color;
	double Size;
	BSTR Brand;

	void Buy() {
		std::cout << "Bought a pair of shoes Id(" << Id << "), Size(" << Size << ")\n";
		std::wcout << "Color(" << Color << "), Brand(" << Brand << ")\n";
	}

};


struct Point
{
	float X;
	float Y;
};

struct MyPolygon
{
	Point* Points;
	int PointCount;
};


struct RegionCollection
{
	int Count;
	MyPolygon* MyPolygons;
};



extern "C" __declspec(dllexport) int Add(int, int);
extern "C" __declspec(dllexport) int GetStringLength(const char*);
extern "C" __declspec(dllexport) const char* CreateStringFromStack();
extern "C" __declspec(dllexport) BSTR ProperlyPassoutString();
extern "C" __declspec(dllexport) double* Return2Double();
extern "C" __declspec(dllexport) void ReleaseDoubleArray(double*);
extern "C" __declspec(dllexport) void PrintShoe(Shoe);
extern "C" __declspec(dllexport) Shoe CreateShoe(double);
extern "C" __declspec(dllexport) MyPolygon CreateRegion();
extern "C" __declspec(dllexport) void ConsumePoints(Point*);




int Add(int a, int b) {
	return a + b;
}

int GetStringLength(const char* text) {
	return strlen(text);
}

// Strings that are allocated on the stack will
// be deallocated after returning,
// thus this is not a viable way to return string
const char* CreateStringFromStack() {
	return "Hello";
}

BSTR ProperlyPassoutString() {
	// SysAllocString accepts a wide char array
	return SysAllocString(L"Hello from CPP");
}

double* Return2Double() {
	auto ptr = new double[2];
	ptr[0] = 100;
	ptr[1] = 200;

	return ptr;
}

void ReleaseDoubleArray(double* ptr) {
	delete[] ptr;
}

void PrintShoe(Shoe shoe) {
	shoe.Buy();
}

Shoe CreateShoe(double param)
{

	Shoe shoe = Shoe();
	shoe.Brand = SysAllocString(L"Peak");
	shoe.Color = SysAllocString(L"Yellow");
	shoe.Id = 1;
	shoe.Size = 100;

	return shoe;
}

MyPolygon CreateRegion()
{
	auto pointCount = 10;
	auto points = new Point[pointCount];
	for (int i = 0; i<10;i++)
	{
		points[i].X = i;
	}


	MyPolygon region = MyPolygon();
	region.PointCount = pointCount;
	region.Points = points;

	return region;
}


 void ConsumePoints(Point* pts)
{
	for (int i = 0; i < 2; ++i)
	{
		auto pt = pts[i];
		std::cout << "P" << i << ".X = " << pt.X << "\n";
	}

}
